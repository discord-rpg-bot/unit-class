# unit-class
Reads and stores data for unitClass.

Port range 8300

Requires [https://gitlab.com/discord-rpg-bot/model](https://gitlab.com/discord-rpg-bot/model) and name server to be running on port 9000

Also needs a mongodb database "unitClassDB" running on port 27017

Runtime documentation [http://localhost:8300/swagger-ui/index.html](http://localhost:8300/swagger-ui/index.html#/)